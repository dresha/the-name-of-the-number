describe('filter convertToWord', function () {
	var testData = [
		[100, 'one hundred'],
		[101, 'one hundred one'],
		[777, 'seven hundred seventy seven'],
		[999, 'nine hundred ninety nine']
	];

	beforeEach(function () {
		module('NameOfNumber');
	});
	it('doesn\'t work for numbers 100-999', inject(function($filter) {
		testData.forEach(function(test){
			expect($filter('convertToWord')(test[0])).toBe(test[1]);
		});
	}));
});