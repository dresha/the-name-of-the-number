describe('filter convertToWord', function () {
	var testData = [
		[10, 'ten'],
		[11, 'eleven'],
		[25, 'twenty five'],
		[50, 'fifty']
	];

	beforeEach(function () {
		module('NameOfNumber');
	});
	it('doesn\'t work for numbers 10-99', inject(function($filter) {
		testData.forEach(function(test){
			expect($filter('convertToWord')(test[0])).toBe(test[1]);
		});
	}));
});