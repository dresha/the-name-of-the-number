describe('filter convertToWord', function () {
	var testData = [
		[0, 'zero'],
		[1, 'one'],
		[2, 'two'],
		[3, 'three'],
		[4, 'four'],
		[5, 'five'],
		[6, 'six'],
		[7, 'seven'],
		[8, 'eight'],
		[9, 'nine']
	];

	beforeEach(function () {
		module('NameOfNumber');
	});
	it('doesn\'t work for digits 0-9', inject(function($filter) {
		testData.forEach(function(test){
			expect($filter('convertToWord')(test[0])).toBe(test[1]);
		});
	}));
});
