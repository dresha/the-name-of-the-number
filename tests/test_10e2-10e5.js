describe('filter convertToWord', function () {
	var testData = [
		[1000, 'one thousand'],
		[1010, 'one thousand ten'],
		[7777, 'seven thousand seven hundred seventy seven'],
		[10000, 'ten thousand'],
		[1000000, 'million']
	];

	beforeEach(function () {
		module('NameOfNumber');
	});
	it('doesn\'t work for numbers 1.000-1.000.000', inject(function($filter) {
		testData.forEach(function(test){
			expect($filter('convertToWord')(test[0])).toBe(test[1]);
		});
	}));
});