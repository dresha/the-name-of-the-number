var gulp = require('gulp');
var karma = require('gulp-karma');
var open = require('gulp-open');
var mainFiles = [
    // Loading angular
    'node_modules/angular/angular.js',
    'node_modules/angular-mocks/angular-mocks.js',

    // App scripts
    './app/app_module.js',
    './app/app_controllers.js',
    './app/app_directives.js',
    './app/app_filters.js',

    // Test file
    './tests/checkFilter.js'
];

var testFiles = [
    {
        name: 'test for digits (0-9)',
        file: './tests/test_0-9.js'
    },
    {
        name: 'test for numerals of tithing (10-99)',
        file: './tests/test_10-99.js'
    },
    {
        name: 'test for numerals of hundreds (100-999)',
        file: './tests/test_100-999.js'
    },
    {
        name: 'test for numerals of thousands and million (1.000-1.000.000)',
        file: './tests/test_10e2-10e5.js'
    }
];

var allTestTask = [];

// Create tests
testFiles.forEach(function(test){
    // Will be run automatically
    allTestTask.push(test.name);

    gulp.task(test.name, function () {
        var testFiles = test.file;
        // Add to loader test file
        mainFiles.push(testFiles);

        // Be sure to return the stream
        return gulp.src(mainFiles).pipe(karma({
            frameworks: ['jasmine'],
            browsers: ['PhantomJS'/*, 'Chrome'*/],

            configFile: 'karma.conf.js',
            action    : 'run'
        })).on('error', function (err) {
            // Make sure failed tests cause gulp to exit non-zero
            throw err;
        });
    });
});

// Watch for changes
gulp.task('watcher', function () {
	gulp.src(testFiles)
        .pipe(karma({
			configFile: 'karma.conf.js',
			action    : 'watch'
		}));
});

// Run server
gulp.task('run server', function() {
    var express = require('express');
    var app = express();
    app.use(express.static(__dirname));
    app.listen(8000);
});

// Open in browser
gulp.task('open in browser', function(){
    var options = {
        url: "http://localhost:8000"
    };

    gulp.src('./index.html')
        .pipe(open('', options));
});

// Run all task
gulp.task('start testing', function(){
    // Start all tests
    gulp.start(allTestTask);

    // Start server
    gulp.start('run server');

    // Open app in browser
    gulp.start('open in browser');
});
