(function (angular) {
    'use strict';
    var app = angular.module('NameOfNumber');

    app.filter('convertToWord', function () {
        return function (n) {
            if(n === null || isNaN(n) || n < 0){
                return;
            }

            var data = {
                simple: ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'],
                teen: ['ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'],
                double: ['', 'switch to teen', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'], // 0 and 1 is not set
                order: [' thousand ', ' million ', ' milliard ', ' billion ', ' trillion ', ' quadrillion ', ' quintillion ', ' sextillion ',' septillion ',' octillion ',' nonillion ',' decillion ',' undecillion ',' duodecillion ',' tredecillion ',' quattuordecillion ',' quindecillion ',' sexdecillion ',' septendecillion ',' octodecillion ',' novemdecillion ',' vigintillion ',' centillion ']
            };
            var numString = n.toString();
            var l = numString.length;
            var first = '';
            var cid = 0;
            var result;
            var actualResult = '';
            var savePreviousResult = '';
            var oid = -1;

            function trim(x) {
                return x.replace(/^\s+|\s+$/gm, '');
            }

            /****************************************
                If you remove these conditions
                you can put number more than million
            ****************************************/
            if(n > 1000000){
                return 'number can\'t be more than million';
            }

            if (n === 1000000) {
                return 'million';
            }
            /****************************************
                            end
             ****************************************/
            while (l--) {
                (function (num) {
                    var orderName = data.order[oid] || '';

                    if (cid === 0) {
                        result = data.simple[num];
                        actualResult = result + orderName + savePreviousResult;
                        first = num;
                    } else if (cid === 1) {
                        if (num === '1') {
                            result = data.teen[first]
                        } else if (num == '0' && first == '0') {
                            result = ''
                        } else if (first == '0') {
                            result = data.double[num]
                        } else if (num !== '0') {
                            result = data.double[num] + ' ' + result
                        }

                        actualResult = result + orderName + savePreviousResult;
                    } else if (cid === 2) {
                        if (num != '0') {
                            result = data.simple[num] + ' hundred ' + result + orderName + savePreviousResult;
                            actualResult = result;
                        }

                        // reset
                        first = '';
                        cid = 0;
                        oid += 1; // add order id
                        savePreviousResult = result;
                        return;
                    }

                    cid += 1;
                }(numString[l]));
            }
            return trim(actualResult);
        };
    });
}(window.angular));