(function (angular) {
    'use strict';
    var app = angular.module('NameOfNumber');

    app.directive('tellMeNumber', function () {
        return {
            link: {
                pre: function(scope, element, attribute){
                    console.log(attribute);
                }
            },
            replace: true,
            restrict: 'E',
            templateUrl: 'app/templates/tell-me-number.html'
        }
    });
}(window.angular));