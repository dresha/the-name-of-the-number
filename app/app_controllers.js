(function (angular) {
    'use strict';
    var options = {
        'empty': {
            mess: 'Please put number from 0 to 1.000.000',
            class: 'alert-info'
        },
        'minus': {
            mess: 'Use only positive numbers !!!',
            class: 'alert-warning'
        },
        'isNotNumber': {
            mess: 'Please use only numbers !!!',
            class: 'alert-danger'
        },
        'isNumber': {
            mess: 'Your result is:',
            class: 'alert-success'
        },
        'moreThanMillion': {
            mess: 'WooH! WooH!',
            class: 'alert-warning'
        }
    };

    angular.module('NameOfNumber').controller('showResult', function($scope, $filter){
        $scope.message = options['empty']['mess'];
        $scope.alertClass = options['empty']['class'];
        $scope.valueWithFilter = '';

        $scope.checkValue = function(){
            var value = $scope.value,
                state = 'isNumber',
                filterValResult = $filter('convertToWord')(value);

            console.log(filterValResult);

            if(value === ''){
                state = 'empty'
            }else if(isNaN(value)){
                state = 'isNotNumber'
            }else if(value < 0 || value[0] === '-'){
                state = 'minus';
                filterValResult = '';
            }else if(filterValResult === "number can't be more than million"){
                state = 'moreThanMillion'
            }
            $scope.valueWithFilter = filterValResult;
            $scope.message = options[state]['mess'];
            $scope.alertClass = options[state]['class'];
        }
    });
}(window.angular));